﻿namespace RestoApp.Models.ViewModels
{
    public class VMResponse
    {
        public int StatusCode { get; set; } = 500;
        public string Message { get; set; }
        public object Data { get; set; }
    }
}
