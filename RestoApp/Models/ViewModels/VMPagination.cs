﻿namespace RestoApp.Models.ViewModels
{
    public class VMPagination<T>
    {
        public int Page { get; set; } = 1;
        public int TotalPage { get; set; } = 1;
        public List<T> Data {  get; set; }
    }
}
