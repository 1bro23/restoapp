﻿using System.ComponentModel.DataAnnotations;

namespace RestoApp.Models.ViewModels
{
    public class VMOrder
    {
        [Key]
        public int? Id { get; set; }
        public int? UserId { get; set; }
        public int? ProductId { get; set; }
        public int? Quantity { get; set; }

        public VMProduct Product { get; set; }
    }
}
