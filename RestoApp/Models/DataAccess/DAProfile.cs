﻿using Microsoft.Data.SqlClient;
using RestoApp.Models.DataModels;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory.Database;
using System.Data;

namespace RestoApp.Models.DataAccess
{
    public class DAProfile
    {
        public static MProfile GetProfileById(string connection,int id)
        {
            var data=new MProfile();
            using (var con = new SqlConnection(connection))
            {
                using (var cmd = new SqlCommand("SP_RestoAppProfileGetById", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Id", id);
                    con.Open();
                    using (var reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            var datas = new object[3];
                            var ndata = reader.GetValues(datas);

                            data.Fullname = datas[0].ToString();
                            data.Image = datas[1].ToString();
                            data.Email = datas[2].ToString();
                        }
                        else throw new Exception("Id not was found");
                    }
                    con.Close();
                }
            }
            return data;
        }
    }
}
