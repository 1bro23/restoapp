﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using RestoApp.Models.DataModels;

namespace RestoApp.Models.DataAccess
{
	public class DAPayment
	{
		public static void Add(string connection,MPayment data)
		{
			using(var con = new SqlConnection(connection))
			{
				using(var cmd = new SqlCommand("SP_AddPayment",con))
				{
					cmd.CommandType = System.Data.CommandType.StoredProcedure;
					cmd.Parameters.AddWithValue("@UserId", data.UserId);
					cmd.Parameters.AddWithValue("@Price",data.Price);
					cmd.Parameters.AddWithValue("@Bank",data.Bank);
					cmd.Parameters.AddWithValue("@VANumber",data.VANumber);
					cmd.Parameters.AddWithValue("@IsPaidOff", data.IsPaidOff);
					con.Open();
					cmd.ExecuteNonQuery();
					con.Close();
				}
			}
		}
		public static List<MPayment> Get(string connection,int UserId)
		{
			var datas = new List<MPayment>();
			using(var con = new SqlConnection(connection))
			{
				using(var cmd = new SqlCommand("SP_GetPayment",con))
				{
					cmd.CommandType = System.Data.CommandType.StoredProcedure;
					cmd.Parameters.AddWithValue("@UserId", UserId);
					con.Open();
					using(var reader = cmd.ExecuteReader())
					{
						while (reader.Read())
						{
							var d = new MPayment();
							foreach(var p in d.GetType().GetProperties())
							{
								var rawData = reader[p.Name];
								p.SetValue(d, rawData,null);
							}
							datas.Add(d);
						}
					}
					con.Close();
				}
			}
			return datas;
		}
	}
}
