﻿using Microsoft.Data.SqlClient;
using RestoApp.Models.DataModels;
using System.Data;
using System.Globalization;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace RestoApp.Models.DataAccess
{
    public class DAProductCategory
    {
        public static void Add(string connection, string? name, int? id)
        {
            if (name != null)
            {
                id = id ?? 1;
                using (var con = new SqlConnection(connection))
                {
                    using (var cmd = new SqlCommand("SP_AddCategory", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@Name", name);
                        cmd.Parameters.AddWithValue("@CreateBy", id);
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                }
            }
        }
        public static List<MProductCategory> Get(string connection, int? id = null, string? name = null)
        {
            var datas = new List<MProductCategory>();

            using (var con = new SqlConnection(connection))
            {
                using (var cmd = new SqlCommand("SP_GetCategory", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    if (id != null) cmd.Parameters.AddWithValue("@Id", id);
                    else if (name != null) cmd.Parameters.AddWithValue("@Name", name);
                    con.Open();
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var data = new MProductCategory();
                            data.Id = (int)reader[0];
                            data.Name = reader[1].ToString();
                            data.CreateOn = reader[2] as DateTime?;
                            data.CreateBy = reader[3] as int?;
                            data.UpdateOn = reader[4] as DateTime?;
                            data.UpdateBy = reader[5] as int?;
                            data.IsDelete = reader[6] as bool?;
                            data.DeleteOn = reader[7] as DateTime?;
                            data.DeleteBy = reader[8] as int?;
                            datas.Add(data);
                        }
                    }
                    con.Close();
                }
            }
            return datas;
        }
        public static void Update(string connection,int? Id,string? Name,int? UserId)
        {
            using (var con = new SqlConnection(connection))
            {
                using(var cmd = new SqlCommand("SP_UpdateCategory", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Id", Id);
                    cmd.Parameters.AddWithValue("@Name", Name);
                    cmd.Parameters.AddWithValue("@UserId", UserId);
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
            }
        }
        public static void Delete(string connection,int Id,int UserId)
        {
            using(var con = new SqlConnection(connection))
            {
                using(var cmd=new SqlCommand("SP_DeleteCategory",con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Id", Id);
                    cmd.Parameters.AddWithValue("@UserId", UserId);
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
            }
        }
    }
}
