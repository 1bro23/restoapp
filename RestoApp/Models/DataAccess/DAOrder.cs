﻿using Microsoft.Data.SqlClient;
using RestoApp.Models.DataModels;
using RestoApp.Models.ViewModels;
using System.Data;

namespace RestoApp.Models.DataAccess
{
    public class DAOrder
    {
        public static List<VMOrder> Get(string connection,int? id=null,int? userId=null,int? productId=null)
        {
            var datas = new List<VMOrder>();
            using(var con = new SqlConnection(connection))
            {
                string Sp="SP_GetOrder";
                using(var cmd=new SqlCommand(Sp, con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@id", id);
                    cmd.Parameters.AddWithValue("@userId", userId);
                    cmd.Parameters.AddWithValue("@productId", productId);
                    con.Open();
                    using(var reader = cmd.ExecuteReader())
                    {
                        while(reader.Read()){
                            var data = new VMOrder();
                            var d = new MOrder();
                            foreach(var prop in d.GetType().GetProperties())
                            {
                                var rawData = reader[prop.Name];
                                var targetProp = data.GetType().GetProperty(prop.Name);
                                targetProp.SetValue(data, rawData, null);
                            }
                            var vmProduct = new VMProduct();
                            var product = DAProduct.Get(connection, id: data.ProductId).FirstOrDefault();
                            if(product != null)
                            {
                                foreach (var prop in product.GetType().GetProperties())
                                {
                                    var rawData = prop.GetValue(product);
                                    var targetProp = vmProduct.GetType().GetProperty(prop.Name);
                                    targetProp.SetValue(vmProduct, rawData, null);
                                }
                                vmProduct.Category = DAProductCategory.Get(connection, vmProduct.CategoryId).FirstOrDefault();
                                data.Product = vmProduct;
                                datas.Add(data);
                            }
                        }
                    }
                    con.Close();
                }
            }
            return datas;
        }
        public static void Update(string connection,int? id,int? userId,int? productId,int? quantity)
        {
            using(var con = new SqlConnection(connection))
            {
                string SP = "SP_UpdateOrder";
                using(var cmd = new SqlCommand(SP, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@id", id);
                    cmd.Parameters.AddWithValue("@userId", userId);
                    cmd.Parameters.AddWithValue("@productId", productId);
                    cmd.Parameters.AddWithValue("@quantity", quantity);
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
            }
        }
    }
}
