﻿using Microsoft.Data.SqlClient;
using RestoApp.Models.DataModels;
using System.Data;

namespace RestoApp.Models.DataAccess
{
    public class DAProduct
    {
        public static void Add(string connection,MProduct data)
        {
            using(var con = new SqlConnection(connection))
            {
                using(var cmd = new SqlCommand("SP_AddProduct", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@CategoryId", data.CategoryId);
                    cmd.Parameters.AddWithValue("@Image", data.Image);
                    cmd.Parameters.AddWithValue("@Name", data.Name);
                    cmd.Parameters.AddWithValue("@Price", data.Price);
                    cmd.Parameters.AddWithValue("@Detail", data.Detail);
                    cmd.Parameters.AddWithValue("@CreateBy", data.CreateBy);
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
            }
        }
        public static void Update(string connection,MProduct data)
        {
            using(var con = new SqlConnection(connection))
            {
                using (var cmd = new SqlCommand("SP_UpdateProduct", con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@UpdateBy",data.UpdateBy);
                    cmd.Parameters.AddWithValue("@Id",data.Id);
                    cmd.Parameters.AddWithValue("@CategoryId",data.CategoryId);
                    cmd.Parameters.AddWithValue("@Image",data.Image);
                    cmd.Parameters.AddWithValue("@Name",data.Name);
                    cmd.Parameters.AddWithValue("@Price",data.Price);
                    cmd.Parameters.AddWithValue("@Detail",data.Detail);
                    cmd.Parameters.AddWithValue("@IsDelete",data.IsDelete);
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
            }
        }
        public static List<MProduct> Get(string connection,int? id=null,int? categoryId=null,string? name=null,int? minPrice=null,int? maxPrice=null)
        {
            var data = new List<MProduct>();
            using(var con = new SqlConnection(connection))
            {
                using(var cmd = new SqlCommand("SP_GetProduct",con))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Id",id);
                    cmd.Parameters.AddWithValue("@CategoryId",categoryId);
                    cmd.Parameters.AddWithValue("@Name",name);
                    cmd.Parameters.AddWithValue("@MinPrice",minPrice);
                    cmd.Parameters.AddWithValue("@MaxPrice",maxPrice);
                    con.Open();
                    using(var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            data.Add(new MProduct()
                            {
                                Id = reader.GetValue(0) as int?,
                                CategoryId = reader.GetValue(1) as int?,
                                Name = reader.GetValue(2) as string,
                                Price = reader.GetValue(3) as int?,
                                Detail = reader.GetValue(4) as string,
                                CreateOn = reader.GetValue(5) as DateTime?,
                                CreateBy = reader.GetValue(6) as int?,
                                UpdateOn = reader.GetValue(7) as DateTime?,
                                UpdateBy = reader.GetValue(8) as int?,
                                Image = reader.GetValue(12) as string
                            });
                        }
                    }
                    con.Close();
                }
            }
            return data;
        }
    }
}
