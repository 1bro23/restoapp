﻿using System.ComponentModel.DataAnnotations;

namespace RestoApp.Models.DataModels
{
    public class MPayment
    {
        [Key]
        public int? Id { get; set; }
        public int? UserId { get; set; }
        public int? Price { get; set; }
        public string? Bank { get; set; }
        public string? VANumber { get; set; }
        public bool IsPaidOff { get; set; }
        public DateTime? CreateOn { get; set; }
        public DateTime? ExpireOn { get; set; }
    }
}
