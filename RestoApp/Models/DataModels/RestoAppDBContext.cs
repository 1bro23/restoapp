﻿using Microsoft.EntityFrameworkCore;

namespace RestoApp.Models.DataModels
{
    public class RestoAppDBContext : DbContext
    {
        public RestoAppDBContext(DbContextOptions<RestoAppDBContext> options) : base(options)
        {
            
        }
        public DbSet<MProfile> Profiles { get; set; } = null;
        public DbSet<MUser> User { get; set; } = null;
        public DbSet<MProduct> Product { get; set; } = null;
        public DbSet<MProductCategory> ProductCategory { get; set; } = null;
        public DbSet<MOrder> Order { get; set; } = null;
        public DbSet<MPayment> MPayments { get; set; } = null;
    }
}
