﻿using System.ComponentModel.DataAnnotations;

namespace RestoApp.Models.DataModels
{
    public class MUser
    {
        [Key]
        public int? Id { get; set; }
        public string? Username { get; set; }
        public string? Password { get; set; }
        public int? ProfileId { get; set; }
        public DateTime? CreateOn { get; set; }
        public int? CreateBy { get; set; }

        public DateTime? UpdateOn { get; set; }
        public int? UpdateBy { get; set; }
        public bool? IsDelete { get; set; } = false;
        public DateTime? DeleteOn { get; set; }
        public int? DeleteBy { get; set; }
    }
}
