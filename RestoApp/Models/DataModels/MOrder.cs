﻿using System.ComponentModel.DataAnnotations;

namespace RestoApp.Models.DataModels
{
    public class MOrder
    {
        [Key]
        public int? Id { get; set; }
        public int? UserId { get; set; }
        public int? ProductId { get; set; }
        public int? Quantity { get; set; }
    }
}
