﻿using System.ComponentModel.DataAnnotations;

namespace RestoApp.Models.DataModels
{
    public class MProductCategory
    {
        [Key]
        public int? Id { get; set; }
        public string? Name { get; set; }
        public DateTime? CreateOn { get; set; }
        public int? CreateBy { get; set; }

        public DateTime? UpdateOn { get; set; }
        public int? UpdateBy { get; set; }
        public bool? IsDelete { get; set; } = false;
        public DateTime? DeleteOn { get; set; }
        public int? DeleteBy { get; set; }
    }
}
