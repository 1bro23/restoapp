﻿using Microsoft.AspNetCore.Mvc;
using RestoApp.Models.DataAccess;
using RestoApp.Models.DataModels;
using RestoApp.Models.ViewModels;

namespace RestoApp.Controllers
{
    public class OrderController : Controller
    {
        string connection;
        public OrderController(IConfiguration config)
        {
            connection = config.GetConnectionString("SqlConnection");
        }
        public VMResponse Get(int? id,int? productId)
        {
            var response = new VMResponse();
            try
            {
                int userId = HttpContext.Session.GetInt32("UserId")??0;
                response.Data = DAOrder.Get(connection, id, userId, productId);
                response.StatusCode = 200;
                response.Message = "success get data";
            }
            catch (Exception e)
            {
                response.Message = e.Message;
            }
            return response;
        }
        [HttpPost]
        public VMResponse Update(int? id,int? productId,int? qty)
        {
            var response = new VMResponse();
            try
            {
                int userId = HttpContext.Session.GetInt32("UserId") ?? 0;
                DAOrder.Update(connection,id, userId, productId, qty);
                response.StatusCode = 200;
                response.Message = "success update";
            }
            catch (Exception e)
            {
                response.Message = e.Message;
            }
            return response;
        }
    }
}
