﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using RestoApp.Models.DataAccess;
using RestoApp.Models.DataModels;
using RestoApp.Models.ViewModels;
using RestoApp.Utils;
using System.Data;
using System.Security.Cryptography;

namespace RestoApp.Controllers
{
    public class CategoryController : Controller
    {
        string connection;
        public CategoryController(IConfiguration config)
        {
            connection = config.GetConnectionString("SqlConnection");
        }
        public IActionResult Crud(string? sortBy,int n=4,int page=1)
        {
            var datas = DAProductCategory.Get(connection);
            if (sortBy != null)
            {
                if (sortBy == "name-asc") datas=datas.OrderBy(e => e.Name).ToList();
                else if (sortBy == "name-dsc") datas=datas.OrderByDescending(e => e.Name).ToList();
            }
            var dataConvert = Pagination.Convert(datas, page,n);
            ViewData["sortBy"] = sortBy;
            return View(dataConvert);
        }
        [HttpPost]
        public VMResponse Add(string? name)
        {
            var response = new VMResponse();
            int userId = HttpContext.Session.GetInt32("UserId").GetValueOrDefault();
            try
            {
                DAProductCategory.Add(connection, name, userId);
                response.StatusCode = 201;
                response.Message = "success added new category";
            }
            catch (Exception err)
            {
                response.Message = err.Message;
            }
            return response;
        }
        [HttpGet]
        public VMResponse Get(int? id,string? name)
        {
            var response = new VMResponse();
            var datas = new List<MProductCategory>();
            try
            {
                datas = DAProductCategory.Get(connection, id, name);
                response.StatusCode=200;
                response.Message = "Succes get product category";
                response.Data = datas;
            }
            catch (Exception err)
            {
                response.Message = err.Message;
            }
            return response;
        }
        public VMResponse Update(int? id,string? name)
        {
            var response = new VMResponse();
            try
            {
                if (id == null || name == null) throw new Exception("id and name must be not null");
                DAProductCategory.Update(connection, id, name, HttpContext.Session.GetInt32("UserId")??1);
                response.StatusCode = 200;
                response.Message = "Update success";
            }
            catch (Exception e)
            {
                response.Message = e.Message;
            }
            return response;
        }
        public VMResponse Delete(int? id)
        {
            var response = new VMResponse();
            int userId = HttpContext.Session.GetInt32("UserId")??1;
            try
            {
                if (id == null) throw new Exception("id is null");
                DAProductCategory.Delete(connection,id.GetValueOrDefault(), userId);
                response.StatusCode = 200;
                response.Message = "Delete success";
            }
            catch (Exception err)
            {
                response.Message = err.Message;
            }
            return response;
        }
    }
}
