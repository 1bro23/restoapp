﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using RestoApp.Models.DataModels;
using RestoApp.Models.ViewModels;
using System.Data;
using System.Text.RegularExpressions;

namespace RestoApp.Controllers
{
    public class SignupController:Controller
    {
        string connection;
        public SignupController(IConfiguration config)
        {
            connection = config.GetConnectionString("SqlConnection");
        }
        public IActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public VMResponse Signup(MUser user,MProfile profile)
        {
            var response = new VMResponse();
            try
            {
                if (user.Username == null || user.Password == null || profile.Fullname == null || profile.Email == null) throw new Exception("null parameter");
                string pattern = @"^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$";
                var match = Regex.Match(profile.Email, pattern);
                if (!match.Success)
                {
                    response.StatusCode = 403;
                    throw new Exception("Email format not valid");
                }
                if (IsUsernameExist(user.Username)) throw new Exception("Username already exist");
                using (var con = new SqlConnection(connection))
                {
                    using(var cmd = new SqlCommand("SP_RestoAppAddUser", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@Username",user.Username);
                        cmd.Parameters.AddWithValue("@Password",user.Password);
                        con.Open();
                        using(var reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                user.Id = (int)reader.GetDecimal(0);
                            }
                        }
                        con.Close();
                    }
                    using(var cmd = new SqlCommand("SP_RestoAppAddProfile", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@Fullname", profile.Fullname);
                        cmd.Parameters.AddWithValue("@Email", profile.Email);
                        cmd.Parameters.AddWithValue("@CreateBy", user.Id);
                        con.Open();
                        using(var reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                profile.Id = (int)reader.GetDecimal(0);
                            }
                        }
                        con.Close();
                    }
                    using(var cmd = new SqlCommand("SP_RestoAppUpdateUser", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@UserId", user.Id);
                        cmd.Parameters.AddWithValue("@ProfileId", profile.Id);
                        cmd.Parameters.AddWithValue("@UpdateBy", user.Id);
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                }
                response.StatusCode = 200;
                response.Message = "Sign Up Success";
            }
            catch (Exception err)
            {
                response.Message = err.Message;
                throw;
            }
            return response;
        }
        [HttpPost]
        public bool IsUsernameExist(string username)
        {
            using(var con = new SqlConnection(connection))
            {
                var cmd = new SqlCommand("SP_RestoAppUserGetUsername",con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Username", username);
                con.Open();
                var reader = cmd.ExecuteReader();
                if (reader.Read()) return true;
                con.Close();
            }
            return false;
        }
    }
}
