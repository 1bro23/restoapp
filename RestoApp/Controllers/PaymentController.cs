﻿using Microsoft.AspNetCore.Mvc;
using RestoApp.Models.DataAccess;
using RestoApp.Models.DataModels;
using RestoApp.Models.ViewModels;

namespace RestoApp.Controllers
{
    public class PaymentController : Controller
    {
        string connection;
        public PaymentController(IConfiguration config)
        {
            connection = config.GetConnectionString("SqlConnection");
        }
        public IActionResult Index()
        {
            int userId=HttpContext.Session.GetInt32("UserId") ?? 0;
            var orders=DAOrder.Get(connection,userId:userId);
            return View(orders);
        }
        [HttpPost]
        public VMResponse Create(MPayment data)
        {
            var response = new VMResponse();
            try
            {
                data.UserId = HttpContext.Session.GetInt32("UserId") ?? 0;
                var rnd = new Random();
                for(int i = 0; i < 11; i++)
                {
                    data.VANumber += rnd.Next(9);
                }
                data.CreateOn = DateTime.Now;
                data.ExpireOn = DateTime.Now.AddHours(1);

                DAPayment.Add(connection, data);
				response.Data = DAPayment.Get(connection, data.UserId.GetValueOrDefault());
                response.StatusCode = 200;
                response.Message = "Success craete payment";
            }
            catch(Exception err)
            {
                response.Message = err.Message;
            }
            return response;
        }
        public IActionResult InProgress()
        {
            int userId = HttpContext.Session.GetInt32("UserId")??0;
            var datas = DAPayment.Get(connection, userId);
            MPayment data = null;
            if (datas.Count > 0)
            {
                if(datas.Last().ExpireOn>DateTime.Now)data = datas.Last();
            }
            var orders = DAOrder.Get(connection, userId:userId);
            foreach(var o in orders)
            {
                DAOrder.Update(connection,o.Id, userId,  o.ProductId, 0);
            }

            return View(data);
        }
        public VMResponse Get(bool last=false)
        {
            var response = new VMResponse();
            try
            {
                int userId = HttpContext.Session.GetInt32("UserId") ?? 0;
                var datas = DAPayment.Get(connection, userId);
                if (last) response.Data = datas.Last();
                else response.Data = datas;
                response.StatusCode = 200;
                response.Message = "success get data";
            }
            catch (Exception err)
            {
                response.Message = err.Message;
            }
            return response;
        }
    }
}