﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using RestoApp.Models.DataAccess;
using RestoApp.Models.DataModels;
using RestoApp.Models.ViewModels;
using System.Data;
using System.Text.RegularExpressions;

namespace RestoApp.Controllers
{
    public class ProfileController : Controller
    {
        string connection;
        private IWebHostEnvironment env;
        public ProfileController(IConfiguration config,IWebHostEnvironment env)
        {
            connection = config.GetConnectionString("SqlConnection");
            this.env = env;
        }
        public IActionResult Index()
        {
            var data = DAProfile.GetProfileById(connection, HttpContext.Session.GetInt32("ProfileId").GetValueOrDefault());
            ViewData["Payments"] = DAPayment.Get(connection, HttpContext.Session.GetInt32("UserId") ?? 0);
            return View(data);
        }
        [HttpPost]
        public VMResponse Update(string? fullname,string? email,IFormFile? image)
        {
            var response=new VMResponse();
            try
            {
                if (email != null)
                {
                    string pattern = @"^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-z]{2,4}$";
                    if (!Regex.Match(email, pattern).Success) throw new Exception("Email format not valid");
                }
                if (image != null)
                {
                    var ext = new string[] { ".jpeg", ".jpg", ".png" };
                    var fileExt = Path.GetExtension(image.FileName);
                    if (!ext.Contains(fileExt)) throw new Exception("file extension not valid");
                }
                using (var con = new SqlConnection(connection))
                {
                    using(var cmd = new SqlCommand("SP_RestoAppUpdateProfile",con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ProfileId",HttpContext.Session.GetInt32("ProfileId"));
                        if(fullname!= null)cmd.Parameters.AddWithValue("@Fullname",fullname);
                        if (email != null) cmd.Parameters.AddWithValue("@Email",email);
                        if (image != null)
                        {
                            string folderPath = Path.Combine(env.WebRootPath, "image");
                            if (!Directory.Exists(folderPath)) Directory.CreateDirectory(folderPath);
                            var profileData = GetProfile();
                            if (profileData.Image != "")
                            {
                                string existPath = Path.Combine(folderPath, profileData.Image);
                                System.IO.File.Delete(existPath);
                            }
                            string uniqueName = Guid.NewGuid().ToString()+'_'+image.FileName;
                            string path = Path.Combine(folderPath, uniqueName);
                            using(var fs=new FileStream(path, FileMode.Create))
                            {
                                image.CopyTo(fs);
                                cmd.Parameters.AddWithValue("@Image", uniqueName);
                            }
                        }
                        cmd.Parameters.AddWithValue("@UpdateBy",HttpContext.Session.GetInt32("UserId"));
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();

                    }
                }
                response.StatusCode = 200;
                response.Message = "Update success";
            }
            catch (Exception e)
            {
                response.Message = e.Message;
            }
            return response;
        }
        public MProfile GetProfile()
        {
            var data = new MProfile();
            if (HttpContext.Session.GetInt32("ProfileId") != null)
            {
                data = DAProfile.GetProfileById(connection,HttpContext.Session.GetInt32("ProfileId").GetValueOrDefault());
            }
            else data = null;
            return data;
        }
    }
}
