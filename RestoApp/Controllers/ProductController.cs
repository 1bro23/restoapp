﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using RestoApp.Models.DataAccess;
using RestoApp.Models.DataModels;
using RestoApp.Models.ViewModels;
using RestoApp.Utils;
using System.Data;

namespace RestoApp.Controllers
{
    public class ProductController : Controller
    {
        private string connection;
        private IWebHostEnvironment env;
        public ProductController(IConfiguration config,IWebHostEnvironment env)
        {
            connection = config.GetConnectionString("SqlConnection");
            this.env = env;
        }
        public IActionResult Index(string? filterName, string? filterCategoryName, int? filterUpperPrice,int? filterLowerPrice,int? page=1)
        {
            ViewData["filterName"] = filterName;
            ViewData["filterCategoryName"] = filterCategoryName;
            ViewData["filterUpperPrice"] = filterUpperPrice;
            ViewData["filterLowerPrice"] = filterLowerPrice;
			var data = DAProduct.Get(connection);
            var productCategory = DAProductCategory.Get(connection);

            if(filterName!=null)data = data.Where(e => e.Name.ToLower().Contains(filterName.ToLower())).ToList();
            if (filterCategoryName != null) data = data.Where(e => productCategory.Where(pc => pc.Id == e.CategoryId).FirstOrDefault().Name.ToLower() == filterCategoryName.ToLower()).ToList();
            if (filterUpperPrice != null) data = data.Where(e => e.Price <= filterUpperPrice).ToList();
            if (filterLowerPrice != null) data = data.Where(e => e.Price >= filterLowerPrice).ToList();


            var vmPage = new VMPagination<MProduct>() { Data=data};
            if (page != null)
            {
                vmPage = Pagination.Convert(data, page.GetValueOrDefault(),8);
                data = vmPage.Data;
            }
            
            return View(vmPage);
        }
        public IActionResult Crud(string? sortBy,int? page = 1)
        {
            var productCategory = DAProductCategory.Get(connection);
            ViewData["ProductCategory"] = productCategory;
            var productRaw = DAProduct.Get(connection);
            if (sortBy != null)
            {
                switch (sortBy)
                {
                    case "category-dsc":
                        productRaw = productRaw.OrderBy(e => productRaw[e.CategoryId.GetValueOrDefault()].Name).ToList();
                        break;
                    case "category-asc":
                        productRaw = productRaw.OrderByDescending(e => productRaw[e.CategoryId.GetValueOrDefault()].Name).ToList();
                        break;
                    case "name-dsc":
                        productRaw = productRaw.OrderBy(e => e.Name).ToList();
                        break;
                    case "name-asc":
                        productRaw = productRaw.OrderByDescending(e => e.Name).ToList();
                        break;
                    case "price-dsc":
                        productRaw = productRaw.OrderBy(e => e.Price).ToList();
                        break;
                    case "price-asc":
                        productRaw = productRaw.OrderByDescending(e => e.Price).ToList();
                        break;
                    case "detail-dsc":
                        productRaw = productRaw.OrderBy(e => e.Detail).ToList();
                        break;
                    case "detail-asc":
                        productRaw = productRaw.OrderByDescending(e => e.Detail).ToList();
                        break;
                }
            }
            var product = Pagination.Convert(productRaw,page??1);
            ViewData["Product"] = product;
            ViewData["SortBy"] = sortBy;
            return View();
        }
        [HttpPost]
        public VMResponse Add(MProduct data,IFormFile image)
        {
            var response = new VMResponse();
            try
            {
                data.CreateBy = HttpContext.Session.GetInt32("UserId").GetValueOrDefault();
                if (image != null)
                {
                    var ext = new string[] { ".jpeg", ".jpg", ".png" };
                    var fileExt = Path.GetExtension(image.FileName);
                    if (!ext.Contains(fileExt)) throw new Exception("file format doesn't match");

                    string folderPath = Path.Combine(env.WebRootPath, "image","product");
                    if (!Directory.Exists(folderPath)) Directory.CreateDirectory(folderPath);
                    string uniqueName = Guid.NewGuid().ToString() + "_" + image.FileName;
                    string filePath = Path.Combine(folderPath, uniqueName);
                    using(var fs = new FileStream(filePath,FileMode.Create))
                    {
                        image.CopyTo(fs);
                    }
                    data.Image = uniqueName;
                }

                DAProduct.Add(connection, data);
                response.StatusCode = 201;
                response.Message = "success add data";
            }
            catch (Exception err)
            {
                response.Message = err.Message;
            }
            return response;
        }
        [HttpPut]
        public VMResponse Update(MProduct data,IFormFile imagefile)
        {
            var response = new VMResponse();
            try
            {
                data.UpdateBy = HttpContext.Session.GetInt32("UserId")??0;
                if (imagefile != null)
                {
                    var ext = new string[] { ".jpeg", ".jpg", ".png" };
                    if (ext.Contains(Path.GetExtension(imagefile.FileName)))
                    {
                        string folderPath = Path.Combine(env.WebRootPath, "image", "product");
                        var getData = DAProduct.Get(connection, id: data.Id).FirstOrDefault();
                        if (getData.Image != null)
                        {
                            string filePath = Path.Combine(folderPath, getData.Image);
                            System.IO.File.Delete(filePath);
                        }
                        string uniqueName = Guid.NewGuid().ToString() + "_" + imagefile.FileName;
                        string newFilePath = Path.Combine(folderPath, uniqueName);
                        using(var fs = new FileStream(newFilePath,FileMode.Create))
                        {
                            imagefile.CopyTo(fs);
                        }
                        data.Image = uniqueName;
                    }
                    else throw new Exception("file extension not supported");
                }

                DAProduct.Update(connection, data);
                response.StatusCode = 200;
                response.Message = "success update";
            }
            catch (Exception e)
            {
                response.Message = e.Message;
            }
            return response;
        }
        public VMResponse Get(int? id, int? categoryId, string? name, int? minPrice, int? maxPrice)
        {
            var response = new VMResponse();
            try
            {
                response.Data = DAProduct.Get(connection, id, categoryId, name, minPrice, maxPrice);
                response.StatusCode = 200;
                response.Message = "Success get data";
            }
            catch (Exception e)
            {
                response.Message = e.Message;
            }
            return response;
        }
        public VMResponse Delete(int? id)
        {
            var response = new VMResponse();
            try
            {
                if (id == null) throw new Exception("id must not be null");
                var data = new MProduct()
                {
                    Id = id,
                    IsDelete = true,
                    UpdateBy = HttpContext.Session.GetInt32("UserId") ?? 0
                };
                DAProduct.Update(connection, data);
                response.StatusCode = 200;
                response.Message = "success delete";
            }
            catch (Exception e)
            {
                response.Message = e.Message;
            }
            return response;
        }
        public IActionResult SearchName()
        {
            return View();
        }
		public IActionResult SearchCategory()
		{
            var productCategory = DAProductCategory.Get(connection);
			return View(productCategory);
		}
		public IActionResult SearchPrice()
		{
			return View();
		}
	}
}
