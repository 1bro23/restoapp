﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using RestoApp.Models.DataModels;
using RestoApp.Models.ViewModels;
using System.Data;

namespace RestoApp.Controllers
{
    public class LoginController:Controller
    {
        private string connection;
        private readonly SignupController signup;
        public LoginController(IConfiguration config)
        {
            connection = config.GetConnectionString("SqlConnection");
            signup = new SignupController(config);
        }
        public IActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public VMResponse Login(MUser user)
        {
            var response = new VMResponse();
            try
            {
                if (!signup.IsUsernameExist(user.Username))
                {
                    response.StatusCode = 401;
                    throw new Exception("Username not exist");
                }
                using(var con = new SqlConnection(connection))
                {
                    using(var cmd=new SqlCommand("SP_RestoAppUserLogin", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@Username", user.Username);
                        cmd.Parameters.AddWithValue("@Password", user.Password);
                        con.Open();
                        using(var reader = cmd.ExecuteReader())
                        {
                            if(reader.Read()) 
                            {
                                HttpContext.Session.SetInt32("UserId", reader.GetInt32(0));
                                HttpContext.Session.SetInt32("ProfileId", reader.GetInt32(1));
                                response.StatusCode = 200;
                                response.Message = "Login success";
                            }
                            else
                            {
                                response.StatusCode = 401;
                                response.Message = "Password doesn't correct";
                            }
                        }
                        con.Close();
                    }
                }
            }
            catch (Exception e)
            {
                response.Message = e.Message;
            }
            return response;
        }
        public IActionResult Logout()
        {
            try
            {
                HttpContext.Session.Remove("UserId");
                HttpContext.Session.Remove("ProfileId");
            }
            catch (Exception e)
            {
            }
            return RedirectToAction("Index","Home");
        }
    }
}
