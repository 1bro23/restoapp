﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;

namespace RestoApp.Controllers
{
    public class SPController : Controller
    {
        string connection;
        public SPController(IConfiguration config)
        {
            connection = config.GetConnectionString("SqlConnection");
        }
        public bool CreateSP()
        {
            try
            {
                using(var con = new SqlConnection(connection))
                {
                    string query = @"
                        create procedure SP_RestoAppAddProfile
                        as 
                        go
                        create procedure SP_RestoAppAddUser
                        as 
                        go
                        create procedure SP_RestoAppUpdateProfile
                        as 
                        go
                        create procedure SP_RestoAppUpdateUser
                        as 
                        go
                        create procedure SP_RestoAppUserGetUsername
                        as 
                        go
                        create procedure SP_RestoAppUserLogin
                        as 
                        go
                        create procedure SP_RestoAppProfileGetById
                        as 
                        go
                        create procedure SP_AddCategory
                        as 
                        go
                        create procedure SP_GetCategory
                        as 
                        go
                        create procedure SP_DeleteCategory
                        as 
                        go
                        create procedure SP_UpdateCategory
                        as 
                        go
                        create procedure SP_AddProduct
                        as 
                        go
                        create procedure SP_UpdateProduct
                        as 
                        go
                        create procedure SP_GetProduct
                        as 
                        go
                        create procedure SP_AddOrder
                        as
                        go
                        create procedure SP_GetOrder
                        as
                        go
                        create procedure SP_UpdateOrder
                        as
                        go
                        create procedure SP_AddPayment
                        as
                        go
                        create procedure SP_GetPayment
                        as
                        go
                    ";
                    using(var cmd = new SqlCommand(query, con))
                    {
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                }
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }
        public bool UpdateSP()
        {
            try
            {
                using(var con=new SqlConnection(connection))
                {
                    string query = @"
                    ALTER procedure [dbo].[SP_RestoAppAddProfile]
                    @Fullname varchar(100),
                    @Email varchar(100),
                    @CreateBy int
                    as
                    insert into Profiles(Fullname,Email,CreateOn,CreateBy,IsDelete)
                    values(@Fullname,@Email,GETDATE(),@CreateBy,0)
                    select SCOPE_IDENTITY() as 'Id'
                    go
                    ALTER procedure [dbo].[SP_RestoAppAddUser]
                    @Username varchar(100),
                    @Password varchar(100)
                    as
                    insert into [User]([Username],[Password],CreateOn,IsDelete)
                    values (@Username,@Password,GETDATE(),0)
                    update [User] set CreateBy = SCOPE_IDENTITY() where Id=SCOPE_IDENTITY()
                    select SCOPE_IDENTITY() as 'Id'
                    go
                    ALTER procedure [dbo].[SP_RestoAppUpdateProfile]
                    @ProfileId int,
                    @Fullname varchar(100)=NULL,
                    @Email varchar(100)=NULL,
                    @Image varchar(100)=NULL,
                    @UpdateBy int
                    as
                    if @Fullname is not null
                    update Profiles set Fullname=@Fullname where Id=@ProfileId
                    if @Email is not null
                    update Profiles set Email=@Email where Id=@ProfileId
                    if @Image is not null
                    update Profiles set Image=@Image where Id=@ProfileId
                    update Profiles set UpdateBy=@UpdateBy,UpdateOn=GETDATE() where Id=@ProfileId
                    go
                    ALTER procedure [dbo].[SP_RestoAppUpdateUser]
                    @UserId int,
                    @Username varchar(100)=NULL,
                    @Password varchar(100)=NULL,
                    @ProfileId int=NULL,
                    @UpdateBy int
                    as
                    if @Username is not null
                    update [user] set Username=@Username where Id=@UserId
                    if @Password is not null
                    update [user] set Password=@Password where Id=@UserId
                    if @ProfileId is not null
                    update [user] set ProfileId=@ProfileId where Id=@UserId
                    update [user] set UpdateBy=@UpdateBy,UpdateOn=GETDATE() where Id=@UserId
                    go
                    ALTER procedure [dbo].[SP_RestoAppUserGetUsername]
                    @Username varchar(100)=null
                    as
                    if @Username is not null
                    select Username
                    from [User] 
                    where Username=@Username
                    else
                    select username
                    from [user]
                    go
                    ALTER procedure [dbo].[SP_RestoAppUserLogin]
                    @Username varchar(100),
                    @Password varchar(100)
                    as
                    select Id,ProfileId from [user] where Username=@Username and Password=@Password
                    go
                    alter procedure SP_RestoAppProfileGetById
                    @Id int
                    as
                    select fullname,image,email
                    from profiles
                    where id=@Id and IsDelete='false'
                    go
                    alter procedure SP_AddCategory
                    @Name varchar(100),
                    @CreateBy int
                    as
                    insert into ProductCategory(Name,CreateBy,CreateOn)
                    values(@Name,@CreateBy,GETDATE())
                    go
                    alter procedure SP_GetCategory
                    @Id int = Null,
                    @Name varchar(100) = Null
                    as
                    if @Id is not null
                    select * from ProductCategory where Id = @Id and (IsDelete = 0 or IsDelete is null)
                    else if @Name is not null
                    select * from ProductCategory where Name like '%'+@Name+'%' and (IsDelete = 0 or IsDelete is null)
                    else
                    select * from ProductCategory where IsDelete = 0 or IsDelete is null
                    go
                    alter procedure SP_DeleteCategory
                    @Id int,
                    @UserId int
                    as
                    update ProductCategory
                    set IsDelete = 1, DeleteBy=@UserId,DeleteOn=Getdate()
                    where Id=@Id
                    go
                    alter procedure SP_UpdateCategory
                    @Id int,
                    @Name varchar(100),
                    @UserId int
                    as
                    update ProductCategory
                    set Name=@Name,UpdateOn=getdate(),UpdateBy=@UserId
                    where Id=@Id
                    go
                    alter procedure SP_AddProduct
                    @CategoryID int,
                    @Image nvarchar(max),
                    @Name varchar(100),
                    @Price int,
                    @Detail varchar(100),
                    @CreateBy int
                    as
                    insert into Product(CategoryId,Image,Name,Price,Detail,CreateOn,CreateBy)
                    values(@CategoryID,@Image,@Name,@Price,@Detail,GETDATE(),@CreateBy)
                    go
                    ALTER procedure [dbo].[SP_UpdateProduct]
                    @UpdateBy int,
                    @Id int,
                    @CategoryId int=null,
                    @Image varchar(100)=null,
                    @Name varchar(100)=null,
                    @Price int=null,
                    @Detail varchar(100)=null,
                    @IsDelete bit=null
                    as
                    if @CategoryId is not null
                    update Product set CategoryId = @CategoryId where id = @Id
                    if @Image is not null
                    update Product set Image = @Image where id= @Id
                    if @Name is not null
                    update Product set Name = @Name where id = @Id
                    if @Price is not null
                    update Product set Price = @Price where id = @Id
                    if @Detail is not null
                    update Product set Detail = @Detail where id = @Id
                    if @IsDelete is not null
                    update Product set IsDelete = @IsDelete where id = @Id
                    if not(@CategoryId is null and @Name is null and @Price is null and @Detail is null and @IsDelete is null)
                    begin
                    update Product set UpdateBy = @UpdateBy where id = @Id
                    update Product set UpdateOn = GETDATE() where id = @Id
                    end
                    go
                    alter procedure SP_GetProduct
                    @Id int = null,
                    @CategoryId int = null,
                    @Name varchar(100) = null,
                    @MinPrice int = null,
                    @MaxPrice int = null
                    as
                    select * from Product
                    where (IsDelete is null or IsDelete = 0)
                    and (@id is null or Id = @Id) 
                    and (@CategoryId is null or CategoryId = @CategoryId) 
                    and (@Name is null or Name like '%'+@Name+'%')
                    and (@MinPrice is null or Price > @MinPrice)
                    and (@MaxPrice is null or Price < @MaxPrice)
                    go
                    alter procedure SP_AddOrder
                    @UserId int,
                    @ProductId int,
                    @Quantity int
                    as
                    insert into 'Order'(UserId,ProductId,Quantity)
                    values(@UserId,@ProductId,@Quantity)
                    go
                    alter procedure SP_GetOrder
                    @Id int =null,
                    @UserId int=null,
                    @ProductId int=null
                    as
                    select * from 'Order'
                    where (@Id is null or Id = @UserId)
                    and (@UserId is null or UserId = @UserId) 
                    and (@ProductId is null or ProductId = @ProductId)
                    go
                    alter procedure SP_UpdateOrder
                    @id int =null,
                    @userId int =null,
                    @productId int=null,
                    @quantity int
                    as
                    if(select 1 from 'Order' where UserId=@userId and ProductId=@productId) = 1
                    update 'Order' set Quantity=@quantity where UserId=@userId and ProductId=@productId
                    else if @userId is not null and @productId is not null
                    insert into 'Order'(UserId,ProductId,Quantity)
                    values(@userId,@productId,@quantity)
                    go
                    alter procedure SP_AddPayment
                    @UserId int,
                    @Price int,
                    @Bank varchar(10),
                    @VANumber varchar(12),
                    @IsPaidOff binary
                    as
                    insert into MPayments(UserId,Price,Bank,VANumber,IsPaidOff,CreateOn,ExpireOn)
                    values(@UserId,@Price,@Bank,@VANumber,@IsPaidOff,GetDate(),DATEADD(hour,1,GetDate()))
                    go
                    alter procedure SP_GetPayment
                    @UserId int
                    as
                    select * from MPayments where UserId = @UserId
                    go
                    ";
                    using(var cmd = new SqlCommand(query, con))
                    {
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                }
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }
    }
}
