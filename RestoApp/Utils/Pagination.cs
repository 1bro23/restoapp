﻿using RestoApp.Models.ViewModels;

namespace RestoApp.Utils
{
    public class Pagination
    {
        public static VMPagination<T> Convert<T>(List<T> data,int page,int n=4)
        {
            var d = new VMPagination<T>();
            int take = n;
            int skip = (page-1) * take;
            d.Page=page;
            d.TotalPage=(take-1+data.Count)/take;
            if (page <= d.TotalPage)
            {
                if ((data.Count - skip) < n) take = data.Count % take;
                d.Data=data.Skip(skip).Take(take).ToList();
            }
            return d;
        }
    }
}
